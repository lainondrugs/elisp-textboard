;; Model for the database.

(defvar *max-threads-on-page* 3)
(defvar *bump-limit* 500)

(defvar *boards* '(("d" . "Discussions")
		   ("a" . "Anime")
		   ("b" . "Random")
		   ("cyb" . "Cyber culture")))

;; board | thread-id | reply-count | last-reply-time
(defvar *threads* '())

;; thread-id | reply-id | text | date-time
(defvar *replies* '())

;; modifiers

(defun list-with-item-added-back (l i)
  (reverse (cons i (reverse l))))

(defun add-thread (board-name text)
  (let* ((time (format-time-string "%Y-%m-%d %H:%M:%S"))
	 (last-reply (car (reverse *replies*)))
	 (last-reply-id (cdr (assoc :reply-id last-reply)))
	 (thread-id (+ 1 last-reply-id)))
    (let ((threads-rec `((:board           . ,board-name)
			 (:thread-id       . ,thread-id)
			 (:reply-count     . 0)
			 (:last-reply-time . ,time)))
	  (replies-rec `((:thread-id . ,thread-id)
			 (:reply-id  . ,thread-id)
			 (:text      . ,text)
			 (:date-time . ,time))))
      (setq *threads* (cons threads-rec *threads*))
      (setq *replies* (list-with-item-added-back *replies* replies-rec))
      (delete-old-threads))))

(defun add-reply (thread-id text)
  (let* ((time (format-time-string "%Y-%m-%d %H:%M:%S"))
	 (last-reply (car (reverse *replies*)))
	 (last-reply-id (cdr (assoc :reply-id last-reply)))
	 (reply-id (+ 1 last-reply-id))
	 (old-thread-rec (car (seq-filter (lambda (rec) (= (cdr (assoc :thread-id rec)) thread-id))
					  *threads*)))
	 (threads-list-without-current-rec (seq-filter (lambda (rec) (not (equal old-thread-rec rec)))
						       *threads*)))
    (let ((threads-rec `((:board           . ,(cdr (assoc :board old-thread-rec)))
			 (:thread-id       . ,thread-id)
			 (:reply-count     . ,(+ 1 (cdr (assoc :reply-count old-thread-rec))))
			 (:last-reply-time . ,time)))
	  (replies-rec `((:thread-id . ,thread-id)
			 (:reply-id  . ,reply-id)
			 (:text      . ,text)
			 (:date-time . ,time))))
      (setq *threads* (cons threads-rec threads-list-without-current-rec))
      (setq *replies* (list-with-item-added-back *replies* replies-rec))
      (delete-old-threads))))

(defun get-all-after (l n)
  "If `l' consist of more then `n' items, return all items starting from n's."
  "Empty list is returned otherwise."
  (if (= n 0)
      l
    (if (>= n (length l))
	'()
      (get-all-after (cdr l) (- n 1)))))

(defmacro filter-out-records (threads)
  `(let ((filtered-thread-ids (mapcar (lambda (rec) (cdr (assoc :thread-id rec)))
				      ,threads)))
     (lambda (rec)
       (let ((rec-thread-id (cdr (assoc :thread-id rec))))
	 (not (member rec-thread-id filtered-thread-ids))))))

(message "%s" (seq-filter (filter-out-records *replies*) *replies*))

(setq lexical-binding t)

(defun delete-old-threads ()
  (dolist (b *boards*)
    (let ((board-name (car b))
	  (the-threads (seq-filter (lambda (rec)
				     (string= (cdr (assoc :board-name rec))
					      board-name))
				   *threads*)))
      (message "Name: %s\nThreads: %s" board-name the-threads)
      (when (> (length the-threads) *max-threads-on-page*)
	(let ((oldest-threads (get-all-after the-threads *max-threads-on-page*)))
	  ;; Remove all replies to these threads.
	  (setq *threads* (seq-filter (filter-out-records oldest-threads) *threads*))
	  ;; Remove all these thread records.
	  (setq *replies* (seq-filter (filter-out-records oldest-threads) *replies*))
	  )))))

(delete-old-threads)
(message "%s" (threads-list "a"))
(message "%s\n%s" *threads* *replies*)

(defun initialize-records ()
  (let ((first-threads-rec `((:board           . "d")
			     (:thread-id       . 0)
			     (:reply-count     . 0)
			     (:last-reply-time . "before time")))
	(first-replies-rec `((:thread-id . 0)
			     (:reply-id  . 0)
			     (:text      . "Sup, here we go. Boards are initialized and users can start using this board.")
			     (:date-time . "before time"))))
    (setq *threads* (list first-threads-rec))
    (setq *replies* (list first-replies-rec))))

;; getters

(defun boards-list ()
  *boards*)

(defun board-description (board-name)
  (cdr (car (seq-filter (lambda (b) (string= (car b) board-name))
			*boards*))))

(defun get-replies-rec-for-thread (thread-id)
  (car
   (seq-filter (lambda (rec)
		 (and
		  (= (cdr (assoc :thread-id rec))
		     (cdr (assoc :reply-id  rec)))
		  (= (cdr (assoc :thread-id rec))
		     thread-id)))
	       *replies*)))

(defun threads-list (board-name)
  (let* ((threads-recs (seq-filter (lambda (rec) (string= (cdr (assoc :board rec)) board-name))
				   *threads*))
	 (threadlist-recs (mapcar (lambda (rec)
				    (let* ((this-thread-id (cdr (assoc :thread-id rec)))
					   (the-replies-rec (get-replies-rec-for-thread this-thread-id)))
				      (append `((:text      . ,(cdr (assoc :text      the-replies-rec)))
						(:date-time . ,(cdr (assoc :date-time the-replies-rec))))
					      rec)))
				  threads-recs)))
    threadlist-recs))

(defun replies-list (thread-id)
  (seq-filter (lambda (rec) (= (cdr (assoc :thread-id rec)) thread-id))
	      *replies*))
