;; A page with the list of existing boards.

(defun board-to-html (brd)
  (concat "<div class=\"brd\"><a href='./" (car brd) "/'>" (car brd) " - " (cdr brd) "</a></div>"))

(defun gen-boards-main-html (boards)
  (concat "<html><head>"
	  "<title>Boards</title>"
	  "<style>"
	  "  .brd {margin: 1em; padding: 1em; border-style: dotted solid;}"
	  "</style>"
	  "</head>"
	  "<body>"
	  "<h1 align='center'>1488 textboard</h1>"
	  "<hr/>"
	  (let ((html-boards
		 (mapcar #'board-to-html boards)))
	    (if (= (length html-boards) 0)
		""
	      (eval `(concat ,@html-boards))))
	  "</body></html>"))

;; Old piece of code no one needs.
;;
;; (defun gen-board-html ()
;;   (concat "<html><head>"
;; 	  "<title>Messages</title>"
;; 	  "<style>"
;; 	  "  .msg {margin: 1em; padding: 1em; border-style: dotted solid;}"
;; 	  "  form {margin: 1em; padding: 1em;}"
;; 	  "</style>"
;; 	  "</head>"
;; 	  "<body>"
;; 	  (let ((html-messages
;; 		 (mapcar (lambda (m)
;; 			   (concat "<div class=\"msg\">" m "</div>"))
;; 			 *messages*)))
;; 	    (if (= (length html-messages) 0)
;; 		""
;; 	      (eval `(concat ,@html-messages))))
;; 	  "<form action='/board/new_msg' method='post'>"
;; 	  "  <input type='text' name='msg'>"
;; 	  "  <input type='submit' value='Submit'>"
;; 	  "</form>"
;; 	  "</body></html>"))
