;;

(defun thread-rec-to-html (rec)
  (concat "<div class=\"post\">"
	  "Time: " (cdr (assoc :date-time rec))
	  " id: " (number-to-string (cdr (assoc :thread-id rec)))
	  " <a href='./" (number-to-string (cdr (assoc :thread-id rec))) "'>[Go to thread]</a>"
	  "<hr/>"
	  (cdr (assoc :text rec))
	  "<hr/>"
	  "Replies: " (number-to-string (cdr (assoc :reply-count rec)))
	  "</div>"))

(defun gen-boards-threads-html (board-name board-description threads)
  (concat "<html><head>"
	  "<title>/" board-name " - Threads</title>"
	  "<style>"
	  "  .post {margin: 1em; padding: 1em; border-style: dotted solid;}"
	  "</style>"
	  "</head>"
	  "<body>"
	  "<h1 align='center'>" board-description "</h1>"
	  "<hr>"
	  (let ((html-threads (mapcar #'thread-rec-to-html threads)))
	    (if (= (length html-threads) 0)
		"No threads."
	      (eval `(concat ,@html-threads))))
	  "<hr/>"
	  "<div align='center'>"
	  "  <p>Create new thread:</p>"
	  "  <form action='/boards/" board-name "/' method='post'>"
	  "    <textarea name='msg' cols='40' rows='5'></textarea>"
	  "    <br/>"
	  "    <input type='submit' value='Post'>"
	  "  </form>"
	  "</div>"
	  "</body></html>"))
