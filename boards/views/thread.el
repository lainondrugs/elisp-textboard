;;

(defun reply-rec-to-html (rec)
  (concat "<div class=\"post\">"
	  "Time: " (cdr (assoc :date-time rec))
	  " id: " (number-to-string (cdr (assoc :reply-id rec)))
	  "<hr/>"
	  (cdr (assoc :text rec))
	  "</div>"))

(defun gen-boards-replies-html (board-name thread-id replies)
  (concat "<html><head>"
	  "<title>Replies to /" board-name "/" (number-to-string thread-id) "</title>"
	  "<style>"
	  "  .post {margin: 1em; padding: 1em; border-style: dotted solid;}"
	  "</style>"
	  "</head>"
	  "<body>"
	  "<h1 align='center'>Replies to /" board-name "/" (number-to-string thread-id) "</h1>"
	  "<hr>"
	  (let ((html-replies (mapcar #'reply-rec-to-html replies)))
	    (if (= (length html-replies) 0)
		"No replies."
	      (eval `(concat ,@html-replies))))
	  "<hr/>"
	  "<div align='center'>"
	  "  <p>Post a reply:</p>"
	  "  <form action='/boards/" board-name "/" (number-to-string thread-id) "' method='post'>"
	  "    <textarea name='msg' cols='40' rows='5'></textarea>"
	  "    <br/>"
	  "    <input type='submit' value='Reply'>"
	  "  </form>"
	  "</div>"
	  "</body></html>"))
