;;

(defun route-get-asdf (request)
  (with-slots (process headers) request
    (ws-response-header process 200 '("Content-type" . "text/plain"))
    (let ((path (cdr (assoc :GET headers))))
      (process-send-string process path))))

(defun route-get-boards (request)
      (with-slots (process headers) request
	(ws-response-header process 200 '("Content-type" . "text/html; charset=utf-8"))
	(let* ((boards (boards-list))
	       (page (gen-boards-main-html boards)))
	  (process-send-string process page))))

(defun route-get-threads (request)
  (with-slots (process headers) request
	(ws-response-header process 200 '("Content-type" . "text/html; charset=utf-8"))
	(let* ((path (cdr (assoc :GET headers)))
	       (board-name (cadr (reverse (split-string path "/"))))
	       (threads (threads-list board-name))
	       (displayed-board-name (concat "/" board-name " - " (board-description board-name)))
	       (page (gen-boards-threads-html board-name displayed-board-name threads)))
	  (process-send-string process page))))

(defun route-get-replies (request)
  (with-slots (process headers) request
	(ws-response-header process 200 '("Content-type" . "text/html; charset=utf-8"))
	(let* ((path (cdr (assoc :GET headers)))
	       (thread-id  (string-to-number (car  (reverse (split-string path "/")))))
	       (board-name                   (cadr (reverse (split-string path "/"))))
	       (replies (replies-list thread-id))
	       (page (gen-boards-replies-html board-name thread-id replies)))
	  (process-send-string process page))))

(defun route-post-new-thread (request)
  (with-slots (process headers) request
    (let* ((text       (cdr (assoc "msg" headers)))
	   (path       (cdr (assoc :POST headers)))
	   (board-name (cadr (reverse (split-string path "/")))))
      (add-thread board-name text)
      (ws-response-header process 200 '("Content-type" . "text/html; charset=utf-8")))))

(defun route-post-reply-in-thread (request)
  (with-slots (process headers) request
    (let* ((text      (cdr (assoc "msg" headers)))
	   (path      (cdr (assoc :POST headers)))
	   (thread-id (string-to-number (car (reverse (split-string path "/"))))))
      (add-reply thread-id text)
      (ws-response-header process 200 '("Content-type" . "text/html; charset=utf-8")))))
