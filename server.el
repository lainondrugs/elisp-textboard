;; Entry point.

(ws-start
 '(((:GET . "^/asdf/*") . route-get-asdf)
   ((:GET . "^/boards/$") . route-get-boards)
   ((:GET . "^/boards/[a-z0-9]+/$") . route-get-threads)
   ((:GET . "^/boards/[a-z0-9]+/[0-9]+$") . route-get-replies)
   ((:POST . "^/boards/[a-z0-9]+/$") . route-post-new-thread)
   ((:POST . "^/boards/[a-z0-9]+/[0-9]+$") . route-post-reply-in-thread))
 1488)
